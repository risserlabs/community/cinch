#!/bin/sh

set -e
sudo true

_main() {
    if [ "$DUMP" = "" ]; then
        echo "fixing permissions..." 1>&2
        _fix_permissions 1>&2
        echo "disabling clustering..." 1>&2
        _disable_clustering 1>&2
        echo "fixing paths..." 1>&2
        _fix_paths 1>&2
        _dump
        echo "cleaning up..." 1>&2
        _cleanup 1>&2
    else
        echo "fixing permissions..."
        _fix_permissions
        echo "disabling clustering..."
        _disable_clustering
        echo "fixing paths..."
        _fix_paths
        echo "dumping data..."
        mkdir -p "$(dirname "$DUMP")"
        _dump > "$DUMP"
        echo "cleaning up..."
        _cleanup
    fi
}

_fix_permissions() {
    sudo chown -R "$USER" "$WORKDIR"
    sudo docker run --rm \
        -v "$WORKDIR/pgdata:$DOCKER_PGDATA" \
        --entrypoint /bin/bash \
        --user root \
        "$IMAGE" \
        -c "chown -R postgres:postgres $DOCKER_PGDATA && chmod -R 700 $DOCKER_PGDATA"
    if [ "$?" != "0" ]; then
        _fail "failed to fix permissions"
    fi
}

_disable_clustering() {
    cat <<EOF > "$WORKDIR/disable_clustering.sh"
#!/bin/sh
set -e
DOCKER_PGDATA="\$1"
if [ -f "\$DOCKER_PGDATA/patroni.dynamic.json" ]; then
    rm -f "\$DOCKER_PGDATA/patroni.dynamic.json"
    rm -f "\$DOCKER_PGDATA/patroni.yaml"
fi
if [ -f "\$DOCKER_PGDATA/PERCONA_CLUSTER" ]; then
    rm -f "\$DOCKER_PGDATA/PERCONA_CLUSTER"
    rm -f "\$DOCKER_PGDATA/grastate.dat"
fi
if [ -f "\$DOCKER_PGDATA/pgpool_node_id" ]; then
    rm -f "\$DOCKER_PGDATA/pgpool_node_id"
fi
if [ -f "\$DOCKER_PGDATA/pgbouncer.ini" ]; then
    rm -f "\$DOCKER_PGDATA/pgbouncer.ini"
fi
sed -i 's/^hot_standby *=.*/hot_standby = off/' "\$DOCKER_PGDATA/postgresql.conf"
sed -i 's/^wal_level *=.*/wal_level = minimal/' "\$DOCKER_PGDATA/postgresql.conf"
sed -i 's/^max_wal_senders *=.*/max_wal_senders = 0/' "\$DOCKER_PGDATA/postgresql.conf"
sed -i 's/^max_replication_slots *=.*/max_replication_slots = 0/' "\$DOCKER_PGDATA/postgresql.conf"
if [ -f "\$DOCKER_PGDATA/recovery.conf" ]; then
    rm -f "\$DOCKER_PGDATA/recovery.conf"
elif [ -f "\$DOCKER_PGDATA/standby.signal" ]; then
    rm -f "\$DOCKER_PGDATA/standby.signal"
fi
rm -f "\$DOCKER_PGDATA/pg_replslot/*"
rm -f "\$DOCKER_PGDATA/pg_stat/*"
rm -f "\$DOCKER_PGDATA/postmaster.pid"
sed -i '/pg_stat_monitor/d' "\$DOCKER_PGDATA/postgresql.conf"
sed -i '/pg_stat_monitor/d' "\$DOCKER_PGDATA/postgresql.auto.conf"
EOF
    sudo docker run --rm \
        -v "$WORKDIR/pgdata:$DOCKER_PGDATA" \
        --entrypoint /bin/sh \
        -v "$WORKDIR/disable_clustering.sh:/tmp/disable_clustering.sh" \
        "$IMAGE" \
        /tmp/disable_clustering.sh "$DOCKER_PGDATA"
}

_fix_paths() {
    cat <<EOF > "$WORKDIR/fix_paths.sh"
#!/bin/sh
set -e
DOCKER_PGDATA="\$1"
ORIGINAL_PGDATA="\$2"
if [ "\$ORIGINAL_PGDATA" = "" ]; then
    ORIGINAL_PGDATA=\$(sed -n "s/^hba_file[[:space:]]*=[[:space:]]*'//p" "\$PGDATA/postgresql.conf" | sed "s/'[[:space:]]*$//" | sed 's|/pg_hba.conf||')
fi
if [ "\$ORIGINAL_PGDATA" = "" ]; then
    echo "failed to find original pgdata directory" 2>&1
    exit 1
fi
update_paths() {
    file="\$1"
    if [ -f "\$file" ]; then
        sed -i "s|\$ORIGINAL_PGDATA|\$DOCKER_PGDATA|g" "\$file"
    fi
}
for _F in \$DOCKER_PGDATA/*.conf; do
    update_paths "\$_F"
done
EOF
    sudo docker run --rm \
        -v "$WORKDIR/pgdata:$DOCKER_PGDATA" \
        --entrypoint /bin/sh \
        -v "$WORKDIR/fix_paths.sh:/tmp/fix_paths.sh" \
        "$IMAGE" \
        /tmp/fix_paths.sh "$DOCKER_PGDATA" "$ORIGINAL_PGDATA"
}

_dump() {
    cat <<EOF > "$WORKDIR/dump.sh"
#!/bin/sh
set -e
DOCKER_PGDATA="\$1"
echo "local all all trust" > "\$DOCKER_PGDATA/pg_hba.conf"
echo "host all all 127.0.0.1/32 trust" >> "\$DOCKER_PGDATA/pg_hba.conf"
echo "host all all ::1/128 trust" >> "\$DOCKER_PGDATA/pg_hba.conf"
su postgres -c "pg_ctl -D \$DOCKER_PGDATA -w start" 1>&2
until su postgres -c "pg_isready -h localhost" 1>&2; do
    sleep 2
done
su postgres -c "pg_dumpall -h localhost"
su postgres -c "pg_ctl -D \$DOCKER_PGDATA -m fast -w stop" 1>&2
EOF
    sudo docker run --rm \
        -v "$WORKDIR/pgdata:$DOCKER_PGDATA" \
        --entrypoint /bin/sh \
        -v "$WORKDIR/dump.sh:/tmp/dump.sh" \
        "$IMAGE" \
        /tmp/dump.sh "$DOCKER_PGDATA"
}

_error() {
    printf "\e[31mError:\e[0m %s\n" "$1" >&2
}

_pgdata() {
    _SEARCH_DIR="$1"
    if [ "$_SEARCH_DIR" = "" ]; then
        _SEARCH_DIR="$(pwd)"
    fi
    _DEPTH="${2:-3}"
    _PGDATA_DIR=""
    _find_pgdata() {
        _DIR="$1"
        if sudo test -f "$_DIR/PG_VERSION"; then
            _PGDATA_DIR="$_DIR"
            return 0
        fi
        return 1
    }
    sudo find "$_SEARCH_DIR" -maxdepth "$_DEPTH" -type d 2>/dev/null | while IFS= read -r _DIR; do
        if _find_pgdata "$_DIR"; then
            realpath "$_PGDATA_DIR"
            return
        fi
    done
}

_pgversion() {
    _PGDATA_DIR="$1"
    if sudo test -f "$_PGDATA_DIR/PG_VERSION"; then
        _PGVERSION="$(sudo cat "$_PGDATA_DIR/PG_VERSION")"
        if [ "$_PGVERSION" = "" ]; then
            _fail "pgversion not found in $_PGDATA_DIR"
        fi
        echo "$_PGVERSION"
    fi
}

_fail() {
    _error "$1"
    exit 1
}

_cleanup() {
    if [ "$_CLEANUP_DONE" = "1" ]; then
        return
    fi
    _CLEANUP_DONE=1
    sudo rm -rf "$WORKDIR" >/dev/null 2>&1 || true
}

_help() {
    echo "Usage: pgdata2dump [options] <pgdata>"
    echo ""
    echo "Options:"
    echo "  -h, --help                  show help"
    echo "  -o, --dump        <path>    path to the dump file"
    echo "  -i, --image       <name>    postgres image name"
    echo "  -w, --workdir     <path>    path to the work directory"
    echo "  --docker-pgdata   <path>    path to the docker pgdata directory"
    echo "  --original-pgdata <path>    path to the original pgdata directory"
    echo ""
    echo "Arguments:"
    echo "  <pgdata>                    path to the PostgreSQL data directory"
}

while test $# -gt 0; do
    case "$1" in
    -h | --help)
        _help
        exit
        ;;
    -o | --dump)
        if [ -z "$2" ]; then
            _error "no dump file specified"
            exit 1
        fi
        DUMP="$2"
        shift 2
        ;;
    -i | --image)
        if [ -z "$2" ]; then
            _error "no postgres image name specified"
            exit 1
        fi
        IMAGE="$2"
        shift 2
        ;;
    -w | --workdir)
        if [ -z "$2" ]; then
            _error "no work directory specified"
            exit 1
        fi
        WORKDIR="$2"
        shift 2
        ;;
    --original-pgdata)
        if [ -z "$2" ]; then
            _error "no original pgdata directory specified"
            exit 1
        fi
        ORIGINAL_PGDATA="$2"
        shift 2
        ;;
    --docker-pgdata)
        if [ -z "$2" ]; then
            _error "no docker pgdata directory specified"
            exit 1
        fi
        DOCKER_PGDATA="$2"
        shift 2
        ;;
    *)
        break
        ;;
    esac
done
if [ "$#" -gt 0 ]; then
    PGDATA="$1"
    shift
fi

if ! command -v docker >/dev/null 2>&1; then
    _fail "docker is not installed"
fi
if ! sudo docker ps >/dev/null 2>&1; then
    _fail "docker daemon is not running"
fi
if [ "$PGDATA" = "" ]; then
    _help
    exit 1
fi
if [ "$WORKDIR" = "" ]; then
    WORKDIR="$(mktemp -d)"
fi
sudo mkdir -p "$WORKDIR" >/dev/null
WORKDIR="$(realpath "$WORKDIR")"
sudo rm -rf "$WORKDIR" >/dev/null 2>&1 || true
if [ -f "$PGDATA" ]; then
    _TAR_PATH="$PGDATA"
    if echo "$_TAR_PATH" | grep -qE '\.(tar|tar\.gz|tgz)$'; then
        sudo mkdir -p "$WORKDIR/archive"
        sudo tar -xvf "$_TAR_PATH" -C "$WORKDIR/archive" 1>&2
        PGDATA="$(_pgdata "$WORKDIR/archive")"
        if [ "$PGDATA" = "" ]; then
            _fail "no pgdata directory found in $_TAR_PATH"
        fi
        sudo mv "$PGDATA" "$WORKDIR/pgdata" 1>&2
        PGDATA="$WORKDIR/pgdata"
    else
        _fail "pgdata is not a pgdata directory or tar archive"
    fi
else
    PGDATA="$(_pgdata "$PGDATA")"
    if [ "$PGDATA" = "" ]; then
        _fail "no pgdata directory found in $(pwd)"
    fi
    sudo cp -R "$PGDATA" "$WORKDIR/pgdata" 1>&2
fi
if [ "$IMAGE" = "" ]; then
    IMAGE="docker.io/postgres:$(_pgversion "$PGDATA")"
fi
if [ "$DOCKER_PGDATA" = "" ]; then
    DOCKER_PGDATA="/var/lib/postgresql/data"
fi

trap '_cleanup' EXIT SIGHUP SIGINT SIGQUIT SIGTERM >/dev/null 2>&1 || true
_main "$@"
