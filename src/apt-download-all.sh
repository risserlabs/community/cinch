#!/bin/sh

mkdir $1
cd $1
apt-get download $1
for p in $(apt-cache depends $1 |
    grep -E 'Depends|Recommends|Suggests' |
    cut -d ':' -f 2,3 |
    sed -e 's|<||g' -e 's|>||g'); do
    apt-get download $p
done
cd -
