#!/bin/sh

INOTIFY_LIMIT="${1:-999999}"

echo $INOTIFY_LIMIT | sudo tee -a /proc/sys/fs/inotify/max_user_watches >/dev/null
echo $INOTIFY_LIMIT | sudo tee -a /proc/sys/fs/inotify/max_queued_events >/dev/null
echo $INOTIFY_LIMIT | sudo tee -a /proc/sys/fs/inotify/max_user_instances >/dev/null
sudo sed -i 's|^fs\.inotify\.max_user_watches=.*||g' /etc/sysctl.conf
sudo sed -i 's|^fs\.inotify\.max_queued_events=.*||g' /etc/sysctl.conf
sudo sed -i 's|^fs\.inotify\.max_user_instances=.*||g' /etc/sysctl.conf
sudo sed -i '/^$/d' /etc/sysctl.conf
echo fs.inotify.max_user_watches=$INOTIFY_LIMIT | sudo tee -a /etc/sysctl.conf >/dev/null
echo fs.inotify.max_queued_events=$INOTIFY_LIMIT | sudo tee -a /etc/sysctl.conf >/dev/null
echo fs.inotify.max_user_instances=$INOTIFY_LIMIT | sudo tee -a /etc/sysctl.conf >/dev/null
sudo sysctl -p
