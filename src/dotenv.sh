#!/bin/sh

DOTENV=$1
if [ "$DOTENV" = "" ]; then
    DOTENV=.env
fi

awk -F= '
BEGIN { inMultiline=0; quoteType="" }
/^[[:space:]]*#/ || /^[[:space:]]*$/ { print; next }
inMultiline && $0 ~ quoteType "$" { print; inMultiline=0; next }
inMultiline { print; next }
($2 ~ /^"[^"]*$/) || ($2 ~ /^'\''[^'\'']*$/) {
    print "export " $0;
    inMultiline=1;
    quoteType = substr($2, 1, 1);
    next
}
{ print "export " $0 }
' $DOTENV
