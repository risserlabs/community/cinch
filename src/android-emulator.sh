#!/bin/sh

EMULATOR="$HOME/Android/Sdk/emulator/emulator"

AVD=$($EMULATOR -list-avds | wofi -S dmenu)
if [ "$AVD" = "" ]; then
    echo "No AVD selected"
    exit 1
fi

xrun $EMULATOR -avd $AVD
