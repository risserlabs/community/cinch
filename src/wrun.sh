#!/bin/sh

export GDK_BACKEND=wayland
export WINIT_UNIX_BACKEND=wayland
export CLUTTER_BACKEND=wayland
export QT_QPA_PLATFORM=wayland
export SDL_VIDEODRIVER=wayland
export MOZ_ENABLE_WAYLAND=1

exec "$@"
