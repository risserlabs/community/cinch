#!/bin/sh

RESOLUTION=$1
RESOLUTION=${RESOLUTION:-1194x834}

swaymsg create_output
swaymsg output "HEADLESS-1" resolution $RESOLUTION
wayvnc -d -v -o HEADLESS-1 0.0.0.0
