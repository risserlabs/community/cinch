#!/bin/sh

_sponge() {
    if which sponge >/dev/null 2>&1; then
        sponge "$@"
    else
        if [ -p /dev/stdin ]; then
            _TMP_FILE=$(mktemp)
            cat >"$_TMP_FILE"
            cat "$_TMP_FILE" >"$1"
            rm -f "$_TMP_FILE"
        fi
        cat "$1"
    fi
}

if [ -f mkpm.json ]; then
    echo "mkpm already migrated"
    exit 1
fi
git add .
git reset --hard
mv Makefile _M
rm -rf .mkpm **/.mkpm >/dev/null
sed 's|.*mkpm.*||g' .gitignore | _sponge .gitignore >/dev/null
awk -v RS='\n{3,}' -v ORS='\n\n' '1' .gitignore | _sponge .gitignore >/dev/null
sed -i -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}' .gitignore
sed 's|^\.mkpm\/.*||g' .gitattributes | awk 'NF {print}' | _sponge .gitattributes >/dev/null
for m in $(find . -name 'Makefile'); do
    sed -i 's|include .*mkpm\.mk||g' "$m" >/dev/null
    sed -i 's|ifneq (,$(MKPM_READY))|include $(MKPM)/mkpm|g' "$m" >/dev/null
    tac "$m" | awk '!x && /endif/ {x=1; next} 1' | tac | _sponge "$m" >/dev/null
    awk -v RS='\n{3,}' -v ORS='\n\n' '1' "$m" | _sponge "$m" >/dev/null
    sed -i -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}' "$m"
done
mkpm init
for p in $(cat mkpm.mk | sed -n '/export MKPM_PACKAGES_DEFAULT := \\/,$p' | sed '/export MKPM_REPO_DEFAULT := \\/q' | tr -d ' \t' | tr -s '\n' | cut -d'=' -f1 | head -n-1 | tail -n +2); do
    mkpm - i $p
done
mv _M Mkpmfile
sed -i 's|include mkpm\.mk||g' Mkpmfile >/dev/null
sed -i 's|ifneq (,$(MKPM_READY))|include $(MKPM)/mkpm|g' Mkpmfile >/dev/null
tac Mkpmfile | awk '!x && /endif/ {x=1; next} 1' | tac | _sponge Mkpmfile >/dev/null
awk -v RS='\n{3,}' -v ORS='\n\n' '1' Mkpmfile | _sponge Mkpmfile >/dev/null
sed -i -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}' Mkpmfile
rm -f mkpm.mk
