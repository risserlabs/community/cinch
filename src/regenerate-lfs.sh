#!/bin/sh

unset git
export _TMP_PATH="${XDG_RUNTIME_DIR:-${TMPDIR:-${TMP:-${TEMP:-/tmp}}}}/regenerate-lfs"

if [ ! -d .git ]; then
    echo "must be in the root of a git project" >&2
    exit 1
fi
if ! which git 2>&1 >/dev/null; then
    echo "git not installed" >&2
    exit 1
fi
if ! which git-lfs 2>&1 >/dev/null; then
    echo "git-lfs not installed" >&2
    exit 1
fi
if ! which git-filter-repo 2>&1 >/dev/null; then
    echo "git-filter-repo not installed" >&2
    exit 1
fi
if ! which rsync 2>&1 >/dev/null; then
    echo "rsync not installed" >&2
    exit 1
fi

lfs_ls_all_files() {
    for c in $(git log --all --pretty=format:'%H'); do
        git lfs ls-files "$c"
    done | cut -d' ' -f3 | sort | uniq
}

ORIGIN=$(git remote get-url origin)
CWD="$(pwd)"
git add -A
git reset --hard
git clean -fxd
rm -rf "$_TMP_PATH"
mkdir -p "$_TMP_PATH"
cp -r .git "$_TMP_PATH"
cd "$_TMP_PATH"
git add -A
git reset --hard
eval $(echo git filter-repo $( (git ls-files && git lfs ls-files) | cut -d' ' -f3 | sort | uniq -u | sed "s|^|--path '|g" | sed "s|$|'|g") --invert-paths --force)
rm -rf .git
cd "$CWD"
eval $(echo git filter-repo $(lfs_ls_all_files | sed "s|^|--path '|g" | sed "s|$|'|g") --invert-paths --force)
rsync -r "$_TMP_PATH/" "$CWD/"
rm -rf "$_TMP_PATH"
git lfs prune --force
git add -A
if ! git remote get-url origin >/dev/null 2>/dev/null; then
    git remote add origin $ORIGIN
fi
if [ "$(git lfs ls-files)" = "" ]; then
    echo "failed to regenerate lfs" >&2
    exit 1
fi
