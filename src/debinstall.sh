#!/bin/sh

sudo true

_TMP=${XDG_RUNTIME_DIR:-${TMPDIR:-${TMP:-${TEMP:-/tmp}}}}/$(echo $1 | md5sum | cut -d' ' -f1)

cleanup() {
    rm -rf $_TMP
}
trap cleanup EXIT

mkdir -p $_TMP
$(curl --version >/dev/null 2>/dev/null && echo curl -Lo || echo wget -O) \
    $_TMP/binary.deb $1

shift

if ! sudo dpkg -i $_TMP/binary.deb $@; then
    sudo apt-get install -f
    sudo dpkg -i $_TMP/binary.deb $@
fi
