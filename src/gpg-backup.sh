#!/bin/sh

if [ "$SECRET" = "" ]; then
    printf "Enter secret: "
    stty -echo
    read SECRET
    stty echo
fi
i=1
gpg --armor --export-secret-keys $1 | \
    openssl enc -aes-256-cbc -base64 -pbkdf2 -iter 10000 -pass pass:$SECRET | \
    {
        qr_text=""
        line_count=0
        while IFS= read -r line; do
            qr_text="$qr_text$line\n"
            line_count=$((line_count + 1))
            if [ "$line_count" -eq 40 ]; then
                echo "$qr_text" | qrencode -o "gpg$i.jpg"
                i=$((i + 1))
                qr_text=""
                line_count=0
            fi
        done
        if [ "$line_count" -ne 0 ]; then
            echo "$qr_text" | qrencode -o "gpg$i.jpg"
        fi
    }
img2pdf gpg*.jpg --pagesize A4 -o gpg.pdf
