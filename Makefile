PACKAGES := $(sort $(patsubst %.sh,%,$(patsubst %.py,%,$(shell ls src))))
UNINSTALL_PACKAGES := $(patsubst %.sh,uninstall/%,$(shell ls src))

.PHONY: $(PACKAGES)
$(PACKAGES):
	@sudo cp src/$@.$$([ -f src/$@.py ] && echo py || echo sh) /usr/local/bin/$@
	@sudo chmod +x /usr/local/bin/$@

.PHONY: $(UNINSTALL_PACKAGES)
$(UNINSTALL_PACKAGES):
	@sudo rm -f /usr/local/bin/$(@F)

.PHONY: install
install: $(PACKAGES)

.PHONY: uninstall
uninstall: $(UNINSTALL_PACKAGES)
